# Build a lemp stack with ansible and Docker

After running all the commands on this readme file, you will get a running docker container with nginx, php-fpm and mysql.

Mysql will have some test data and there will also be an index.php file showing the employees that are Male which birth date is 1965-02-01 and the hire date is greater than 1990-01-01 ordered by the Full Name of the employee.

The following commands have been tested in Ubuntu 16.04 kvm virtual machine.

## Install doker repository
~~~~
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo deb https://apt.dockerproject.org/repo ubuntu-xenial main | sudo tee /etc/apt/sources.list.d/docker.list

sudo apt-get update
~~~~

## Install docker and required packages
~~~~
sudo apt-get install docker-engine python python-pip libffi-dev libssl-dev python-dev git -y
~~~~

## Install ansible-container
~~~~
wget https://github.com/ansible/ansible-container/archive/release-0.3.0.tar.gz
tar xfz release-0.3.0.tar.gz
cd ansible-container-release-0.3.0/
python setup.py install && cd -
~~~~

## Setup docker to allow access via port and socket
~~~~
cat - <<EOF | sudo tee /etc/systemd/system/docker.service
[Service]
ExecStart=/usr/bin/docker daemon -H 0.0.0.0:2375 -H unix:///var/run/docker.sock
EOF
~~~~

## Restart the engine
~~~~
sudo systemctl daemon-reload && sudo service docker restart
~~~~

## Clone this repository
~~~~
git clone https://bitbucket.org/gekkon/perceptyx && cd perceptyx
~~~~

## Build docker image
~~~~
export COMPOSE_HTTP_TIMEOUT=500
ansible-container build
~~~~

## Run docker image
~~~~
did=`docker run -d perceptyx-lemp`
~~~~

## Check webapp results
~~~~
docker exec -it $did /bin/bash
w3m -dump http://localhost/
~~~~

At this point, the w3m command will output the list of employees.

Done!!
